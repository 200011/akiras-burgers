var pedido = [];
var id_producto = new Map();
var id_precio = new Map();

function add(id){
  pedido.push(id);
  ons.notification.toast(`Has añadido un ${id_producto[id]} a tu cesta`, { buttonLabel: 'OK', timeout: 2000 })
}


window.fn = {};

window.fn.toggleMenu = function () {
  document.getElementById('appSplitter').right.toggle();
};

window.fn.loadView = function (index) {
  document.getElementById('sidemenu').close();
  document.querySelector('#myNavigator').pushPage(index);
  if(index.includes("cesta")){
    
    setTimeout(() => {
      loadCesta();
    }, 500);
  }
};

function loadCesta(){
  
    var i = 0;
    var total = 0;
    while(i < pedido.length){
      var id = pedido[i];
      var total = total + id_precio[id];
      document.getElementById("tablaCesta").innerHTML += `<tr> <th>${id_producto[id]}</th> <td>${id_precio[id]}€</td> </tr>`;
      i++;
    }
    document.getElementById("tablaCesta").innerHTML += `<tr> <th style="color: red">TOTAL</th> <th>${total}€</th> </tr>`;
  
}

window.fn.loadLink = function (url) {
  window.open(url, '_blank');
};

window.fn.pushPage = function (page, anim) {
  if (anim) {
    document.getElementById('appNavigator').pushPage(page.id, { data: { title: page.title }, animation: anim });
  } else {
    document.getElementById('appNavigator').pushPage(page.id, { data: { title: page.title } });
  }
};

document.addEventListener('init', function(event) {
  var page = event.target;

  if (page.id === 'inicio') {
    page.querySelector('#push-button').onclick = function() {
      document.querySelector('#myNavigator').pushPage('page2.html', {data: {title: 'Page 2'}});
    };
  } else if (page.id === 'page2') {
    page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
  }
});
id_producto['h1'] = "Big Burger";
id_precio['h1'] = 12;
id_producto['h2'] = "Cabrera";
id_precio['h2'] = 10;
id_producto['h3'] = "Doble queso";
id_precio['h3'] = 9;
id_producto['h4'] = "Vegana";
id_precio['h4'] = 9;
id_producto['bo1'] = "El de Siempre";
id_precio['bo1'] = 3;
id_producto['bo2'] = "Carnoso";
id_precio['bo2'] = 4;
id_producto['bo3'] = "Iberico";
id_precio['bo3'] = 8;
id_producto['b1'] = "Agua";
id_precio['b1'] = 1;
id_producto['b2'] = "Cola Loca";
id_precio['b2'] = 2;
id_producto['b3'] = "Papsycola";
id_precio['b3'] = 2;
id_producto['b4'] = "Nestaya";
id_precio['b4'] = 2;
id_producto['b5'] = "Zumito";
id_precio['b5'] = 3;